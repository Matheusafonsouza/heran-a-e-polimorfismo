#include "paralelogramo.hpp"
#include <iostream>
#include <math.h>

void Paralelogramo::set_lado(float lado){
    this->lado = lado;
}

float Paralelogramo::get_lado(){
    return this->lado;
}

Paralelogramo::Paralelogramo(){
    set_tipo("Paralelogramo");
    set_base(10);
    set_altura(10);
    set_lado(10);
}

Paralelogramo::Paralelogramo(float base){
    set_tipo("Paralelogramo");
    set_base(base);
    set_altura(base);
    set_lado(base);
}

Paralelogramo::Paralelogramo(float base,float altura,float lado){
    set_tipo("Paralelogramo");
    set_base(base);
    set_altura(altura);
    set_lado(lado);
}

float Paralelogramo::calcula_perimetro(){
    return 2*(get_base()+get_lado());
}