#include "pentagono.hpp"
#include <iostream>
#include <math.h>

Pentagono::Pentagono(){
    set_tipo("Pentagono");
    set_base(10);
    set_altura(10);
}

Pentagono::Pentagono(float base,float altura){
    set_tipo("Pentagono");
    set_base(base);
    set_altura(altura);
}

float Pentagono::calcula_area(){
    return 5*((get_altura()*get_base())/2);
}

float Pentagono::calcula_perimetro(){
    return 5*get_base();
}