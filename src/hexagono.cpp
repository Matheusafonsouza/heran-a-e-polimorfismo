#include "hexagono.hpp"
#include <iostream>
#include <math.h>

Hexagono::Hexagono(){
    set_tipo("Hexagono");
    set_base(10);
    set_altura(10);
}

Hexagono::Hexagono(float base,float altura){
    set_tipo("Hexagono");
    set_base(base);
    set_altura(altura);
}

float Hexagono::calcula_area(){
    return 6*((get_altura()*get_base())/2);
}

float Hexagono::calcula_perimetro(){
    return 6*get_base();
}